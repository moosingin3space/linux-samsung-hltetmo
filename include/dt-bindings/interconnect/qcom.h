/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Qualcomm interconnect IDs
 *
 * Copyright (c) 2018, Linaro Ltd.
 * Author: Georgi Djakov <georgi.djakov@linaro.org>
 */

#ifndef __DT_BINDINGS_INTERCONNECT_QCOM_H
#define __DT_BINDINGS_INTERCONNECT_QCOM_H

#define BIMC_SNOC_MAS			1
#define BIMC_SNOC_SLV			2
#define MASTER_AMPSS_M0			3
#define MASTER_BLSP_1			4
#define MASTER_CRYPTO_CORE0		5
#define MASTER_DEHR			6
#define MASTER_GRAPHICS_3D		7
#define MASTER_JPEG			8
#define MASTER_LPASS			9
#define MASTER_MDP_PORT0		10
#define MASTER_QDSS_BAM			11
#define MASTER_QDSS_ETR			12
#define MASTER_SDCC_1			13
#define MASTER_SDCC_2			14
#define MASTER_SNOC_CFG			15
#define MASTER_SPDM			16
#define MASTER_TCU_0			17
#define MASTER_TCU_1			18
#define MASTER_USB_HS			19
#define MASTER_VFE			20
#define MASTER_VIDEO_P0			21
#define PNOC_INT_0			22
#define PNOC_INT_1			23
#define PNOC_M_0			24
#define PNOC_M_1			25
#define PNOC_SLV_0			26
#define PNOC_SLV_1			27
#define PNOC_SLV_2			28
#define PNOC_SLV_3			29
#define PNOC_SLV_4			30
#define PNOC_SLV_8			31
#define PNOC_SLV_9			32
#define PNOC_SNOC_MAS			33
#define PNOC_SNOC_SLV			34
#define SLAVE_AMPSS_L2			35
#define SLAVE_BIMC_CFG			36
#define SLAVE_BLSP_1			37
#define SLAVE_BOOT_ROM			38
#define SLAVE_CAMERA_CFG		39
#define SLAVE_CATS_128			40
#define SLAVE_CLK_CTL			41
#define SLAVE_CRYPTO_0_CFG		42
#define SLAVE_DEHR_CFG			43
#define SLAVE_DISPLAY_CFG		44
#define SLAVE_EBI_CH0			45
#define SLAVE_GRAPHICS_3D_CFG		46
#define SLAVE_IMEM_CFG			47
#define SLAVE_LPASS			48
#define SLAVE_MPM			49
#define SLAVE_MSM_PDM			50
#define SLAVE_MSM_TCSR			51
#define SLAVE_MSS			52
#define SLAVE_OCMEM_64			53
#define SLAVE_PMIC_ARB			54
#define SLAVE_PNOC_CFG			55
#define SLAVE_PRNG			56
#define SLAVE_QDSS_CFG			57
#define SLAVE_QDSS_STM			58
#define SLAVE_RBCPR_CFG			59
#define SLAVE_RPM_MSG_RAM		60
#define SLAVE_SDCC_1			61
#define SLAVE_SDCC_4			62
#define SLAVE_SECURITY			63
#define SLAVE_SERVICE_SNOC		64
#define SLAVE_SNOC_CFG			65
#define SLAVE_SPDM			66
#define SLAVE_SYSTEM_IMEM		67
#define SLAVE_TLMM			68
#define SLAVE_USB_HS			69
#define SLAVE_VENUS_CFG			70
#define SNOC_BIMC_0_MAS			71
#define SNOC_BIMC_0_SLV			72
#define SNOC_BIMC_1_MAS			73
#define SNOC_BIMC_1_SLV			74
#define SNOC_INT_0			75
#define SNOC_INT_1			76
#define SNOC_INT_BIMC			77
#define SNOC_MM_INT_0			78
#define SNOC_MM_INT_1			79
#define SNOC_MM_INT_2			80
#define SNOC_MM_INT_BIMC		81
#define SNOC_PNOC_MAS			82
#define SNOC_PNOC_SLV			83
#define SNOC_QDSS_INT			84
#define SYSTEM_SLAVE_FAB_APPS		85

#endif
